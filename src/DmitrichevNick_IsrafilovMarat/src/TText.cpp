#pragma once

#include "TText.h"

int TText::CurrentTextLevel = 0;

TText::TText(PTTextLink pl) {
	if (pl == NULL)
		pl = new TTextLink();
	pCurrent = pFirst = pl;
}

int TText::GoFirstLink(void) {
	while (!Path.empty())
		Path.pop();
	pCurrent = pFirst;
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else
		SetRetCode(TextOK);
	return RetCode;
}

int TText::GoDownLink(void) {
	SetRetCode(TextError);
	if (pCurrent != NULL)
		if (pCurrent->pDown != NULL) {
			Path.push(pCurrent);
			pCurrent = pCurrent->pDown;
			SetRetCode(TextOK);
		}
	return RetCode;
}

int TText::GoNextLink(void) {
	SetRetCode(TextError);
	if (pCurrent != NULL)
		if (pCurrent->pNext != NULL) {
			Path.push(pCurrent);
			pCurrent = pCurrent->pNext;
			SetRetCode(TextOK);
		}
	return RetCode;
}

int TText::GoPrevLink(void) {
	if (Path.empty())
		SetRetCode(TextNoPrev);
	else {
		pCurrent = Path.top();
		Path.pop();
		SetRetCode(TextOK);
	}
	return RetCode;
}

string TText::GetLine(void) {
	if (pCurrent == NULL)
		return string("");
	else
		return string(pCurrent->Str);
}

void TText::SetLine(string s) {
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else
		strncpy(pCurrent->Str, s.c_str(), TextLineLength);
	pCurrent->Str[TextLineLength - 1] = '\0';
}

void TText::InsDownLine(string s) {
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else {
		PTTextLink curDOWN = pCurrent->pDown;
		PTTextLink newDOWN = new TTextLink("", curDOWN, NULL);
		strncpy(newDOWN->Str, s.c_str(), TextLineLength);
		newDOWN->Str[TextLineLength - 1] = '\0';
		pCurrent->pDown = newDOWN;
		SetRetCode(TextOK);
	}
}

void TText::InsDownSection(string s) {
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else {
		PTTextLink curDOWN = pCurrent->pDown;
		PTTextLink newDownSection = new TTextLink("", NULL, curDOWN);
		strncpy(newDownSection->Str, s.c_str(), TextLineLength);
		newDownSection->Str[TextLineLength - 1] = '\0';
		pCurrent->pDown = newDownSection;
		SetRetCode(TextOK);
	}
}

void TText::InsNextLine(string s) {
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else {
		TStr newS;
		strcpy_s(newS, s.c_str());
		pCurrent->pNext = new TTextLink(newS, pCurrent->pNext, NULL);
		SetRetCode(TextOK);
	}
}

void TText::InsNextSection(string s) {
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else {
		TStr newS;
		strcpy_s(newS, s.c_str());
		pCurrent->pNext = new TTextLink(newS, NULL, pCurrent->pNext);
		SetRetCode(TextOK);
	}
}

void TText::DelDownLine(void) {
	SetRetCode(TextOK);
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else if (pCurrent->pDown == NULL)
		SetRetCode(TextError);
	else{
		PTTextLink pl1 = pCurrent->pDown;
		PTTextLink pl2 = pl1->pNext;
		if (pl1->pDown == NULL)
			pCurrent->pDown = pl2;
	}
}

void TText::DelDownSection(void) {
	SetRetCode(TextOK);
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else if (pCurrent->pDown == NULL)
		SetRetCode(TextError);
	else {
		PTTextLink pl1 = pCurrent->pDown;
		PTTextLink pl2 = pl1->pNext;
		pCurrent->pDown = pl2;
	}
}

void TText::DelNextLine(void) {
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else if (pCurrent->pNext == NULL)
		SetRetCode(TextNoNext);
	else if (pCurrent->pNext->IsAtom())
		pCurrent->pNext = pCurrent->pNext->pNext;
}

void TText::DelNextSection(void) {
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else if (pCurrent->pNext == NULL)
		SetRetCode(TextNoNext);
	else
		pCurrent->pNext = pCurrent->pNext->pNext;
}

int TText::Reset(void) {
	while (!St.empty())
		St.pop();
	pCurrent = pFirst;
	if (pCurrent != NULL) {
		St.push(pCurrent);
		if (pCurrent->pNext != NULL)
			St.push(pCurrent->pNext);
		if (pCurrent->pDown != NULL)
			St.push(pCurrent->pDown);
	}
	return IsTextEnded();
}

int TText::IsTextEnded(void) const {
	return !St.size();
}

int TText::GoNext(void) {
	if (!IsTextEnded()) {
		pCurrent = St.top();
		St.pop();
		if (pCurrent != pFirst) {
			if (pCurrent->pNext != NULL)
				St.push(pCurrent->pNext);
			if (pCurrent->pDown != NULL)
				St.push(pCurrent->pDown);
		}
	}
	return IsTextEnded();
}

PTTextLink TText::GetFirstAtom(PTTextLink pl) {
	PTTextLink tmp = pl;
	while (!tmp->IsAtom()) {
		St.push(tmp);
		tmp = tmp->GetDown();
	}
	return tmp;
}

void TText::Print() {
	CurrentTextLevel = 0;
	PrintText(pFirst);
}

void TText::PrintText(PTTextLink ptl) {
	if (ptl != NULL) {
		for (int i = 0; i < CurrentTextLevel; i++)
			cout << "   ";
		cout << ptl->Str << endl;
		CurrentTextLevel++;
		PrintText(ptl->GetDown());
		CurrentTextLevel--;
		PrintText(ptl->GetNext());
	}
}

void TText::Read(char* pFileName) {
	ifstream TxtFile(pFileName);
	CurrentTextLevel = 0;
	if (TxtFile) 
		pFirst = ReadText(TxtFile);
}

PTTextLink TText::ReadText(ifstream& TxtFile) {
	string buf;
	PTTextLink ptl = new TTextLink();
	PTTextLink tmp = ptl;
	while (!TxtFile.eof())
	{
		getline(TxtFile, buf);
		if (buf!="" && buf.front() == '}')
			break;
		else if (buf!= "" && buf.front() == '{')
			ptl->pDown = ReadText(TxtFile);
		else
		{
			while (buf[0] == '\t')
				buf.erase(0, 1);
			ptl->pNext = new TTextLink((char*)(buf.c_str()));
			ptl = ptl->pNext;
		}
	}
	ptl = tmp;
	if (tmp->pDown == nullptr)
	{
		tmp = tmp->pNext;
		delete ptl;
	}
	return tmp;
}

void TText::PrintTextFile(PTTextLink ptl, ofstream & TxtFile)         // ������ ������ �� ����� ptl
{
	if (ptl != NULL) {
		for (int i = 0; i < CurrentTextLevel; i++) TxtFile << "  ";
		TxtFile << "  " << ptl->Str << endl;
		CurrentTextLevel++;
		PrintTextFile(ptl->GetDown(), TxtFile);
		CurrentTextLevel--;
		PrintTextFile(ptl->GetNext(), TxtFile);
	}
}

void TText::Write(char * pFileName) {
	CurrentTextLevel = 0;
	ofstream TextFile(pFileName);
	PrintTextFile(pFirst, TextFile);
}

PTText TText::getCopy() {
	PTTextLink pl1, pl2 = NULL, pl = pFirst, cpl = NULL;

	if (pFirst != NULL) {
		while (!St.empty()) St.pop();
		while (true)
		{
			if (pl != NULL) {  // search for first atom
				pl = GetFirstAtom(pl);
				St.push(pl);
				pl = pl->GetDown();
			}
			else if (St.empty()) break;
			else {
				pl1 = St.top(); St.pop();
				if (strstr(pl1->Str, "Copy") == NULL) {  // first step
					pl2 = new TTextLink("Copy", pl1, cpl);
					St.push(pl2);
					pl = pl1->GetNext();
					cpl = NULL;
				}
				else {  // second step
					strcpy_s(pl1->Str, pl1->pNext->Str);
					pl1->pNext = cpl;
					cpl = pl1;
				}
			}
		}
	}
	return new TText(cpl);
}
